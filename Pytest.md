## **Pytest**框架学习笔记

* #### 安装pytest&allure

  `pip install pytest`

  `pip install allure-pytest`

* #### Pytest执行的方式

  * ##### IDE里面点击方法前的运行

    * Pycharm设置Pytest单元测试执行方式
      * Setting-Tools-Python Integrated Tools改成pytest运行方式

  * ```Python
    if __name__ == '__main__':
        pytest.main('-vs')
    ```

  * ##### 命令行的方式

    * pytest 模块名
      * -v 打印详细信息
      * -s 将打印信息(`print()`)输出到控制台
      * -k 执行包含关键字的测试用例
        * `pytest -k test_a`
          * 名称包含’test_a‘的都会被执行
      * -x 遇到执行失败的用例则停止执行

* #### Pytest参数化

  `@pytest.mark.parametrize`('a,b', [1, 1], [2, 2])

  * 参数包含两部分，argnames&argvalues
    * argnames的数据类型可以是str / list / tuple
    * argvalues的数据类型为list[]，list中可以嵌套list[[], []]&tuple[(), ()]

* #### Pytest装饰器

  * ##### @pytest.fixture

  ```python
  # -*- coding:utf-8 -*-
  '''
  fixture方法不能写return，return代表结束函数从而达不到参数化的效果
  '''
  import pytest
  
  @pytest.fixture()
  def login(a, b):
      c = a + b
      yield c
  
  class TestPra:
  
      def test_01(self, login):
          print(f'{login}')
  ```

  * 可以单独于某个用例执行特定的方法

    * autouse属性可以默认给所有测试用例使用，类似于setup，fixture不加autouse属性就需要单独在每条测试用例中添加fixture的函数----最好不用

      `@pytest.fixture(autouse=True)`

    * scope属性可以指定fixture的作用域，默认为function

      ``@pytest.fixture(scope='module')`

    * yield可以添加到fixture方法里面，实现teardown的作用

      * yield返回了值之后的步骤算teardown

      ```python
      @pytest.fixture()
      def login():
          print('setup')
          yield read_yaml()
          # 开始Teardown操作
          print('teardown')
      ```

      测试用例调用逻辑：yield前面的代码 - 测试用例 - yield后面的代码

      --setup-show可以查看yield顺序

    * params&ids可以向fixture函数传递参数，可以代替参数化parametrize

      `@pytest.fixture(params=[[1, 2], [3, 4]], ids=[1, 2])`

      不可以做open文件的操作

      ```
      def read_yaml():
          with open(r'../test_yaml/test.yaml') as f:
              return yaml.safe_load(f)
      
      
      @pytest.fixture(scope='module', params=read_yaml()['add']['success'], ids=read_yaml()['add']['successIds'])
      def get_add(request):
          result = request.param
          print(result[0])
      ```

  * Fixture方法传参

    * 先定义fixture方法

    ```python
    # 定义driver实例对象,yield返回了值之后执行teardown
    @pytest.fixture(scope='module')
    def get_driver(request):
        driver = webdriver.Chrome()
        print(request.param[0])
        logging.info(request.param[0])
        driver.get(request.param[0])
        # 全局设置隐式等待
        driver.implicitly_wait(3)
        yield driver
        driver.quit()
    ```

    * 定义测试方法

    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/23 23:32
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_iframe.py
    # @software: PyCharm
    # 文件注释  : iframe元素调用
    # 测试地址  : https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable
    import logging
    import pytest
    import logging
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    
    
    class TestFrame(object):
    
        """
        Fixture方法传参：
            通过parametrize把fixture方法传入，要传入fixture方法的参数当argvalues传入([[]])，测试方法再调用fixture方法
            添加indirect=True参数是为了把login当成一个函数去执行，而不是一个参数。
        """
        url = [['https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable']]
        @pytest.mark.parametrize('get_driver', url, indirect=True)
        def test_frame(self, get_driver):
            get_driver.switch_to_frame('iframeResult')
            logging.info('Switch成功')
    
    
    if __name__ == '__main__':
        pytest.main()
    ```

  * @pytest.mark.usefixture()

    这种方式无法获取函数的返回值

  * conftest文件

    * 可以存放一些公共的fixture方法

    * 存放位置

      最好放到项目根目录，寻找原则是深度优先

    * 使用时不需要导入

  * 指定Case执行顺序

    `pip install pytest-ordering`

    * 执行

    ​	`@pytest.mark.run(order=1)`

    ​	可以指定Case执行的顺序，不指定的话pytest默认执行顺序为从上到下

  * 并发执行----不能指定Case顺序、Case不可存在依赖关系

    `pip insatll pytest-xdist`

    - 运行方式

      命令行执行`pytest -n number`

      - eg：`pytest -n 4`

      - 参数auto可以自动分配

        `pytest -n auto`

  * 失败重跑

    `pip install pytest-rerunfailure`

* #### YAML

  * ##### yaml安装
  
    ​	`pip instll pyYAML`
  
  * ##### yaml文件读取
  
  ```python
  def read_yaml():
      with open(r'F:\hogwarts-pytest\test_yaml\test.yaml') as f:
          return yaml.safe_load(f)
  ```

- #### Allure

  - ##### Allure安装和配置

    - 下载Alure压缩包并解压
    - 环境变量配置

  - ##### Python第三方库安装

    `pip install allure-pytest`

  - ##### 运行

    - 在测试执行期间收集结果

      `pytest [测试文件] --alluredir=dirpath`

      --alluredir用于指定存储测试结果的路径

    - 查看测试报告

      - 测试完成后查看实际报告，在线看报告，会直接打开默认浏览器展示当前报告

        `allure serve ./report`

      - 从结果生成报告，启动一个tomcat服务，需要两步：生成报告、打开报告

        - 生成报告

          `allure generate ./report -o ./report/ --clean`

          - ./report代表pytest生成的临时json文件

          - -o代表指定一个report文件具体的存放地址，不指定会自动生成到根路径的allure-report

          - --clean代表覆盖当前文件的结果

        - 打开报告

          `allure open -h 127.0.0.1 -p 8888 ./allure-report`

  - ##### 装饰器

    - @allure.feature

      给测试类标注是什么模块的测试

      ```python
      @allure.feature("Pytest练习")
      class TestPractice:
      
          def setup_class(self):
              self.cal = Calculator()
      ```

      `pytest --allure-feature="Pytest练习"`

    - @allure.story

      给测试方法标注具体的测试功能

      ```python
      @allure.story("成功的除法")
      @pytest.mark.div
      @pytest.mark.parametrize('a,b,expect', read_yaml()['div']['success'], ids=read_yaml()['div']['successIds'])
      def test_div_success(self, a, b, expect):
          assert expect == self.cal.div(a, b)
      ```

      `pytest --allure-story="成功的除法"`

    - @allure.step

      标注具体的测试步骤

      ```python
      @allure.story("成功的加法")
      @pytest.mark.add
      @pytest.mark.parametrize('a,b,expect', read_yaml()['add']['success'], ids=read_yaml()['add']['successIds'])
      def test_add_success(self, a, b, expect):
          with allure.step("成功的加法运算"):
              assert expect == self.cal.add(a, b)
      ```

    - @allure.testcase

      关联测试用例的链接

      ```python
      TEST_CASE_LINK = "https://www.baidu.com"
      @allure.testcase(TEST_CASE_LINK, "加法运算测试用例链接")
      @allure.story("成功的加法")
      @pytest.mark.add
      @pytest.mark.parametrize('a,b,expect', read_yaml()['add']['success'], ids=read_yaml()['add']['successIds'])
      def test_add_success(self, a, b, expect):
          with allure.step("成功的加法运算"):
              assert expect == self.cal.add(a, b)
      ```

    - @allure.serverity

      - 在类、方法、函数上面加

        `@allure.serverity(allure.serverity_level.TRIVIAL)`

      - 执行时

        ​	`pytest --allure-serverities="normal,critical"`

    - @allure.title

      - 给测试方法重命名，在报告的suite展示重命名的标题

        `@allure.story("成功的加法")`

    - @allure.attach

      在报告中加入HTML、图片、视频、文本，写在方法内，图片和视频要调用file方法

      `allure.attach.file("./pic.JPG", "海绵宝宝", attachment_type=allure.attachment_type.JPG)`

      `allure.attach("这是一个文本", "文本", attachment_type=allure.attachment_type.TEXT)`
      
    - allure.raise
    
      抛出异常，类似try...except

- #### Selenium

  - ##### 安装

    - 安装并配置Python环境

    - `pip install selenium`或者Pycharm设置中安装

    - driver下载&解压，安装与当前浏览器版本一致的driver(浏览器版本在设置中查看)

      https://pypi.org/project/selenium/  ----需要翻墙

      https://npm.taobao.org/mirrors/chromedriver/  ----淘宝镜像

      - 解压完成执行./chromedriver.exe
      - MAC执行~./bash_profile最后一行配置环境变量
      - Windows配置目录到环境变量

  - ##### 使用

    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/21 22:51
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_selenium.py
    # @software: PyCharm
    # 文件注释  : selenium的练习
    import time
    import selenium
    from selenium import webdriver
    
    
    class TestSelenium(object):
    
        def test_selenium(self):
            driver = webdriver.Chrome()
            driver.get('https://www.baidu.com')
            time.sleep(3)
    
    
    if __name__ == '__main__':
        test = TestSelenium()
        test.test_selenium()
    ```

    - 初始化&**隐式等待**实例对象可以放到setup，对象关闭放到teardown

      ```python
      def setup(self):
          self.driver = webdriver.Chrome()
          self.driver.get('https://www.baidu.com')
          # setup加入隐式等待的对象实例
          self.driver.implicitly_wait(5)
          pass
      
      def teardown(self):
          self.driver.quit()
      ```

    - 简单的测试用例编写

      - 打开百度->搜索'Sun'->点击第二条搜索记录
      - 使用By.ID需要导入`from selenium.webdriver.common.by import By`

      ```python
      def test_selenium(self):
          """
          使用By.ID需要导入
          from selenium.webdriver.common.by import By
          """
          self.driver.find_element(By.ID, 'kw').send_keys('Sun')
          self.driver.find_element(By.ID, 'su').click()
          # 含有<a/>可以使用By.By.LINK_TEXT
          self.driver.find_element(By.LINK_TEXT, '太阳石油[SUN]美股实时行情 - 富途牛牛').click()
          time.sleep(5)
      ```
      
    - **显示等待**
    
      ```python
      import pytest
      from selenium.webdriver.common.by import By
      from selenium.webdriver.support.ui import WebDriverWait
      from selenium.webdriver.support import expected_conditions
      
      
      class TestWait:
      
          def test_pra(self, get_driver):
              """
              源码：
              def find_element(self, by=By.ID, value=None):
              
              编译器会提示调换参数位置，不能替换，会报错找不到元素
              :return:
              """
              get_driver.find_element(By.ID, 'kw').send_keys('Sun')
              get_driver.find_element(By.ID, 'su').click()
              # By方法单独在一个括号里
              WebDriverWait(get_driver, 10).until(expected_conditions.presence_of_element_located((By.LINK_TEXT, 'SUN(美国互联网技术服务公司) - 百度百科')))
      
      
      if __name__ == '__main__':
          pytest.main()
      ```
    
    - ##### WEB页面定位方式
    
      - Xpath
    
        具体某一个元素
    
        ​	`//*[@id="kw"]`&`//*[@id="kw"]//a`
    
        - a代表属性
        - /仅代表子元素
        - //代表所有子孙元素
    
        获取最后一个元素 / 倒数第二个
    
        ​	`//*[@id="kw"]//a[last()]`&`//*[@id="kw"]//a[last()-1]`
    
      - CSS
    
        不支持原生页面，可以定位**webview**(H5)
    
        $('[name=kw]')
    
        - .代表Class
        - #代表ID
        - ＞代表子元素
        - 空格代表所有子孙元素
        - *代表所有元素