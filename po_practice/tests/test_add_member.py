# -*- coding: UTF-8 -*-
# @Time    : 2021/6/27 14:59
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_add_member.py
# @software: PyCharm
# 文件注释  :
import pytest

from hogwarts_pytest.po_practice.po.main_page import MainPage

class TestAddMember(object):

    def setup(self):
        self.main = MainPage()

    def teardown(self):
        self.main.driver.quit()

    def test_add_member(self):
        self.main.click_contact().click_add_member().add_member().get_member_name()


if __name__ == '__main__':
    pytest.main()
