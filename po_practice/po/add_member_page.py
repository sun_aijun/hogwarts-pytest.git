# -*- coding: UTF-8 -*-
# @Time    : 2021/6/27 14:58
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : add_member_page.py
# @software: PyCharm
# 文件注释  :
"""
E   ImportError: cannot import name 'ContactPage' from 'po_practice.po.contact_page' (F:\Practice\po_practice\po\contact_page.py)
"""
import logging
from selenium.webdriver.common.by import By
from hogwarts_pytest.po_practice.po.base_page import BasePage


class AddMemberPage(BasePage):
    _USERNAME = (By.ID, 'username', '张二狗')
    _ENGLISHRNAME = (By.ID, 'memberAdd_english_name', '二狗子')
    _ACOOUNT = (By.ID, 'memberAdd_acctid', '17636330720')
    _PHONE = (By.ID, 'memberAdd_phone', '17636330720')
    _TITLE = (By.ID, 'memberAdd_title', '搬砖')
    _CHECKBOX = (By.XPATH, '//*[@class="ww_checkbox"]')

    def add_member(self):
        # 可以在函数中import来解决循环调用的报错
        from hogwarts_pytest.po_practice.po.contact_page import ContactPage
        # option = webdriver.ChromeOptions()
        # option.debugger_address = '127.0.0.1:9222'
        # self.driver = webdriver.Chrome(options=option)
        # self.driver.implicitly_wait(10)
        # 输入信息
        self.driver.save_screenshot('./username.png')
        self.find_send(*self._USERNAME)
        self.find_send(*self._ENGLISHRNAME)
        self.find_send(*self._ACOOUNT)
        self.find_send(*self._PHONE)
        self.find_send(*self._TITLE)
        self.execute('document.documentElement.scrollTop=1000')
        self.driver.save_screenshot('./Screen/ClickCheckBox.png')
        checkbox = self.finds(*self._CHECKBOX)
        logging.info(checkbox)
        # 拿到elements列表然后进行操作
        checkbox[4].click()
        # with allure.step('点击保存'):
        #     driver.find_element(By.LINK_TEXT, '保存').click()
        return ContactPage(self.driver)
