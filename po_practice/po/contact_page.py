# -*- coding: UTF-8 -*-
# @Time    : 2021/6/27 14:58
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : contact_page.py
# @software: PyCharm
# 文件注释  :
import time
from selenium.webdriver.common.by import By
from hogwarts_pytest.po_practice.po.add_member_page import AddMemberPage
from hogwarts_pytest.po_practice.po.base_page import BasePage


class ContactPage(BasePage):
    _ADDMEMBER = (By.XPATH, '//*[@class="js_has_member"]/div[1]/a[1]')

    def click_add_member(self):
        # option = webdriver.ChromeOptions()
        # option.debugger_address = '127.0.0.1:9222'
        # self.driver = webdriver.Chrome(options=option)
        # self.driver.implicitly_wait(10)
        time.sleep(5)
        # 点击添加成员
        self.driver.save_screenshot('./Screen/ClickAdd.png')
        # 是否被点击的参数不能穿find_element,直接传定位方式
        self.wait_click(self._ADDMEMBER)
        try:
            while 1:
                self.find_click(*self._ADDMEMBER)
        except :
            AddMemberPage(self.driver)
        return AddMemberPage(self.driver)

    def get_member_name(self):
        pass