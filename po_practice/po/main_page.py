# -*- coding: UTF-8 -*-
# @Time    : 2021/6/27 14:58
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : main_page.py
# @software: PyCharm
# 文件注释  :
from selenium.webdriver.common.by import By

from hogwarts_pytest.po_practice.po.base_page import BasePage
from hogwarts_pytest.po_practice.po.contact_page import ContactPage


class MainPage(BasePage):
    _CONTACT = (By.ID, 'menu_contacts')

    def click_contact(self):
        # option = webdriver.ChromeOptions()
        # option.debugger_address = '127.0.0.1:9222'
        # self.driver = webdriver.Chrome(options=option)
        # self.driver.implicitly_wait(10)
        """
        tuple：*代表解包
        :return:
        """
        self.driver.get('https://work.weixin.qq.com/wework_admin/frame')
        self.find(*self._CONTACT).click()
        # self.driver.find_element(By.ID, 'menu_contacts').click()
        return ContactPage(self.driver)
