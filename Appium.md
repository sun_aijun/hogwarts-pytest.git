# Appium

#### AppiumServer环境配置

- JAVA1.8版本安装&配置环境变量
- AndroidSDK解压&配置环境变量

- Node.js安装(推荐使用12版本)

  - 安装完成配置环境变量

    - CMD确认环境变量

      `node -v`

- AppiumPythonClient第三方库安装

  `pip install AppiumPythonClient` 或者到编译器的设置中安装

- Appium Doctor安装

  - 安装appium-doctor检测appium安装环境

    `npm install -g appium-doctor`

    `appium-doctor`

#### Appium使用

- ##### 服务启动

  `appium --session-override -a 127.0.0.1 -p 4723`

- ##### 导入依赖

  `from appium import webdriver`

- ##### Capability设置 --  值为json格式

  - app apk地址

  - appPackage 包名

  - appActivity  Activity名字

    `adb logcat ActivityManager:I | grep 'cmp'`

    `adb shell am start -W -n <package-name>/<activity-name> -s`

  - udid  指定设备去执行

  - automationName  默认使用uiautomator2(IOS默认使用XCUITest)

  - noRest&fullRest  是否在测试前后重置相关环境

  - unicodeKeyBoard&resetKeyBoard  是否需要输入非英文之外的语言并在测试完成后重置输入法

  - platformName  设备系统-Android/IOS

  - platformVersion 系统型号

  - dontStopAppOnReset  首次启动的时候不停止app(调试或运行期间可以提升运行速度)

  - skipDeviceInitialization  跳过安装，权限设置操作(调试或运行期间可以提升运行速度)

  - settings[waitForIdTimeOut]  只有动态页面才需要设置这个idle超时时间

- 初始化driver

  `python webdriver.remote('http://127.0.0.1:4735', cas)`

- 定位方式

  - Xpath(速度慢，定位灵活)

    `driver.find_element(MobileBy.XPATH, "//*[@user='user']")`

    - 常用表达式
  
      - 逻辑运算符(not、and、or)	
    
        `driver.find_element(MobileBy.XPATH, "//*[contains(@text, 'user') and contains(@text, 'pwd')]")`
    
      - 表达式(contains、ends_with、starts_with)
    
        `driver.find_element(MobileBy.XPATH, "//*[contains(@text, 'user')]")`
    
  - ID定位(优先级最高)
  
  - CSS定位
  
  - Accessibility(content-desc)
  
  - UIAutomator(仅安卓可用)
  
  - predicate(仅IOS可用)

- 参数模拟

  - GitHub-Faker

    ```python
    from faker import Faker
    
    # 实例化时不传参数产生的数据为英文
    # fake = Faker()
    fake = Faker(locale='zh_CN')
    print(fake.name())
    print(fake.phone_number())
    ```

- PO模型

  - Page、Case、Utils

  - base_page复用driver

    `self.driver.launch_app()`