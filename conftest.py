# -*- coding:utf-8 -*-
import logging
"""
conftest文件用于存放公告模块
1.fixture方法若有返回值必须写yield，否则达不到参数化的需求
2.SetUp -> yield -> TearDown
"""
import pytest
import logging
from selenium import webdriver
from tested_code.calculator import Calculator

# 定义driver实例对象,yield返回了值之后执行teardown
@pytest.fixture(scope='module')
def get_driver(request):
    # 封装多浏览器的启动
    try:
        if 'Firefox' in request.param[1]:
            driver = webdriver.Firefox()
        else:
            driver = webdriver.Chrome()
    except IndexError:
        raise IndexError(logging.info('----Browser参数未传入----'))
    try:
        # 通过request传入url
        logging.info(request.param[0])
        driver.get(request.param[0])
    except IndexError:
        raise IndexError(logging.info('URL为空'))
    # 设置窗口最大化
    driver.maximize_window()
    # 全局设置隐式等待
    driver.implicitly_wait(3)
    yield driver
    driver.quit()

# 定义TouchActions的driver获取
@pytest.fixture(scope='module')
def touch_driver():
    # 实例化option
    option = webdriver.ChromeOptions()
    option.add_experimental_option('w3c', False)
    # 将option传入driver
    driver = webdriver.Chrome(options=option)
    driver.get('https://www.baidu.com')
    yield driver
    driver.quit()

# 定义计算器的实例化函数，作用域为class级别
@pytest.fixture(scope='class')
def init_cal():
    cal = Calculator()
    yield cal

# 定义开始计算的方法
@pytest.fixture(scope='function')
def start_cal():
    logging.info("===开始计算===")
    yield logging.info("===计算中===")
    logging.info("===结束计算===")
