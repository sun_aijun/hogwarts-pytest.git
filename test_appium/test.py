# -*- coding: UTF-8 -*-
# @Time    : 2021/7/4 15:38
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test.py
# @software: PyCharm
# 文件注释  :
from faker import Faker

# 实例化时不传参数产生的数据为英文
# fake = Faker()
fake = Faker(locale='zh_CN')
print(fake.name())
print(fake.phone_number())
