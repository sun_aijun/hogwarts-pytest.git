# 接口学习

- #### tcpdump + Wireshark

  - 记录Log

    `sudo tcpdump host www.baidu.com -w /tmp/tcpdump.log`

    - Windows需要使用WinPcap + WinDump

      - 安装

        - http://itindex.net/detail/55574-windump-%E6%96%B9%E6%B3%95

        - 添加windump到环境变量

      - 使用

        https://www.cnblogs.com/congyinew/p/12594814.html

        - 打开Git执行命令

          `windump -Al -w ./tmp/tcpdump.log host www.baidu.com`

        - 打开另一个Git执行

          `curl www.baidu.com`

        - 打开WireShark打开日志文件

  - 请求接口

    `curl http://www.baidu.com`

  - 使用Wireshark进行Log分析

- #### cURL命令

  - get请求

    `curl $url`

  - post请求

    `curl -d $url`

  - proxy使用

    `curl -x 'http://127.0.0.1:8080' $url`

  - ##### 重要参数

    - `-H` 消息头设置

    - `-u` 用户认证

    - `-d` 要发送的post数据@file 表示来自于文件

    - `--data-urlencode` 对内容进行url编码

    - `-G` 把data数据当成get请求的参数发送，常与--data-urlencode结合使用

    - `-o` 写入文件

    - `-x` 代理http代理 sockt5代理

    - `-v` verbose打印更详细的日志

    - `-s` 关闭一些提示输出

    - ` | jq` 以json格式输出

      windows需要安装js.exe

