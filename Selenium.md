#### Selenium

- ##### 安装

  - 安装并配置Python环境

  - `pip install selenium`或者Pycharm设置中安装

  - driver下载&解压，安装与当前浏览器版本一致的driver(浏览器版本在设置中查看)

    https://pypi.org/project/selenium/  ----需要翻墙

    https://npm.taobao.org/mirrors/chromedriver/  ----淘宝镜像

    - 解压完成执行./chromedriver.exe
    - MAC执行~./bash_profile最后一行配置环境变量
    - Windows配置目录到环境变量

- ##### 使用

  ```python
  # -*- coding: UTF-8 -*-
  # @Time    : 2021/6/21 22:51
  # @Author  : Sun
  # @Email   : xxxxxxxxxxx@xxx.com
  # @File    : test_selenium.py
  # @software: PyCharm
  # 文件注释  : selenium的练习
  import time
  import selenium
  from selenium import webdriver
  
  
  class TestSelenium(object):
  
      def test_selenium(self):
          driver = webdriver.Chrome()
          driver.get('https://www.baidu.com')
          time.sleep(3)
  
  
  if __name__ == '__main__':
      test = TestSelenium()
      test.test_selenium()
  ```

  - 初始化&**隐式等待**实例对象可以放到setup，对象关闭放到teardown

    ```python
    def setup(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com')
        # setup加入隐式等待的对象实例
        self.driver.implicitly_wait(5)
        pass
    
    def teardown(self):
        self.driver.quit()
    ```

  - 简单的测试用例编写

    - 打开百度->搜索'Sun'->点击第二条搜索记录
    - 使用By.ID需要导入`from selenium.webdriver.common.by import By`

    ```python
    def test_selenium(self):
        """
        使用By.ID需要导入
        from selenium.webdriver.common.by import By
        """
        self.driver.find_element(By.ID, 'kw').send_keys('Sun')
        self.driver.find_element(By.ID, 'su').click()
        # 含有<a/>可以使用By.By.LINK_TEXT
        self.driver.find_element(By.LINK_TEXT, '太阳石油[SUN]美股实时行情 - 富途牛牛').click()
        time.sleep(5)
    ```

  - **显示等待**

    ```python
    import pytest
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions
    
    
    class TestWait:
    
        def test_pra(self, get_driver):
            """
            源码：
            def find_element(self, by=By.ID, value=None):
            
            编译器会提示调换参数位置，不能替换，会报错找不到元素
            :return:
            """
            get_driver.find_element(By.ID, 'kw').send_keys('Sun')
            get_driver.find_element(By.ID, 'su').click()
            # By方法单独在一个括号里
            WebDriverWait(get_driver, 10).until(expected_conditions.presence_of_element_located((By.LINK_TEXT, 'SUN(美国互联网技术服务公司) - 百度百科')))
    
    
    if __name__ == '__main__':
        pytest.main()
    ```

- ##### WEB页面定位方式

  - Xpath

    具体某一个元素

    ​	`//*[@id="kw"]`&`//*[@id="kw"]//a`

    - a代表属性
    - /仅代表子元素
    - //代表所有子孙元素

    获取最后一个元素 / 倒数第二个

    ​	`//*[@id="kw"]//a[last()]`&`//*[@id="kw"]//a[last()-1]`

  - CSS

    不支持原生页面，可以定位**webview**(H5)

    $('[name=kw]')

    - .代表Class
    - #代表ID
    - ＞代表子元素
    - 空格代表所有子孙元素
    - *代表所有元素

- ##### Actions

  官方文档：https://selenium-python.readthedoc.io/api.html

  测试地址：https://sahitest.com/demo/clicks.htm

  - ActionChains

    执行PC端的鼠标点击、双击、右键、拖拽等事件

    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/23 9:07
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_actionchain.py
    # @software: PyCharm
    # 文件注释  : Actions操作
    import time
    import pytest
    from selenium.webdriver.common.by import By
    from selenium.webdriver import ActionChains
    
    
    class TestActions(object):
    
        def test_actionchains(self, get_driver):
            # 实例化Action
            action = ActionChains(get_driver)
            # 收集action对象
            action.send_keys_to_element(get_driver.find_element(By.ID, 'kw'), 'Sun')
            action.click(get_driver.find_element(By.ID, 'su'))
            # 调用peform执行操作
            action.perform()
            time.sleep(5)
    
    
    
    if __name__ == '__main__':
        pytest.main()
    ```
  - TouchActions
  
    模拟PC和移动端的点击、滑动、拖拽、多点触控
  
    ```python
    """
    TouchAction的调用需要先实例化option，然后传入driver对象，否则会报错
    """
    # 定义TouchActions的driver获取
    @pytest.fixture(scope='module')
    def touch_driver():
        # 实例化option
        option = webdriver.ChromeOptions()
        option.add_experimental_option('w3c', False)
        # 将option传入driver
        driver = webdriver.Chrome(options=option)
        driver.get('https://www.baidu.com')
        yield driver
        driver.quit()
    ```
  
    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/23 9:07
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_actionchain.py
    # @software: PyCharm
    # 文件注释  : Actions操作
    import time
    import pytest
    from selenium.webdriver.common.by import By
    from selenium.webdriver import TouchActions
    
    
    class TestActions(object):
    
        def test_touchaction(self, touch_driver):
            # 实例化Action
            action = TouchActions(touch_driver)
            # 收集action对象
            action.scroll_from_element(touch_driver.find_element(By.ID, 'kw'), 10000, 0)
            # 调用peform执行操作
            action.perform()
            time.sleep(5)
    
    
    if __name__ == '__main__':
        pytest.main()
    ```

- 多窗口切换

  ```python
  # -*- coding: UTF-8 -*-
  # @Time    : 2021/6/23 22:50
  # @Author  : Sun
  # @Email   : xxxxxxxxxxx@xxx.com
  # @File    : test_windows.py
  # @software: PyCharm
  # 文件注释  : selenium的多窗口处理
  import time
  import logging
  import pytest
  from selenium.webdriver.common.by import By
  
  
  class TestWindows(object):
  
      def test_windows(self, get_driver):
          get_driver.find_element(By.ID, 's-top-loginbtn').click()
          # current_window_handle方法获取当前所有窗口
          logging.info(get_driver.window_handles)
          # current_window_handle方法获取当前窗口
          logging.info(get_driver.current_window_handle)
          get_driver.find_element(By.CLASS_NAME, 'pass-reglink').click()
          # info操作拼接可以用格式化方式传入
          logging.info(f'打开注册页面后的窗口列表:{get_driver.window_handles}')
          logging.info(f'打开注册页面后当前所处的窗口:{get_driver.current_window_handle}')
          # 跳转到注册窗口
          get_driver.switch_to_window(get_driver.window_handles[-1])
          get_driver.find_element(By.ID, 'TANGRAM__PSP_4__userName').send_keys('张铁锤')
          get_driver.find_element(By.ID, 'TANGRAM__PSP_4__phone').send_keys('15235535702')
          # 切换回首页
          get_driver.switch_to_window(get_driver.window_handles[0])
          time.sleep(3)
  
  
  
  if __name__ == '__main__':
      pytest.main()
  ```
  
- ##### iframe布局跳转

  ```python
  # -*- coding: UTF-8 -*-
  # @Time    : 2021/6/23 23:32
  # @Author  : Sun
  # @Email   : xxxxxxxxxxx@xxx.com
  # @File    : test_iframe.py
  # @software: PyCharm
  # 文件注释  : iframe元素调用
  # 测试地址  : https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable
  import logging
  import pytest
  import logging
  from selenium import webdriver
  from selenium.webdriver.common.by import By
  
  
  class TestFrame(object):
  
      """
      Fixture方法传参：
          通过parametrize把fixture方法传入，要传入fixture方法的参数当argvalues传入([[]])，测试方法再调用fixture方法
          添加indirect=True参数是为了把login当成一个函数去执行，而不是一个参数。
      """
      url = [['https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable']]
      @pytest.mark.parametrize('get_driver', url, indirect=True)
      def test_frame(self, get_driver):
          # 进入iframe布局
          get_driver.switch_to_frame('iframeResult')
          logging.info('Switch进入成功')
          # 跳出iframe布局，switch_to_default_content代表进入此页面的默认布局
          get_driver.switch_to_default_content()
          logging.info('Switch退出成功')
  
  
  if __name__ == '__main__':
      pytest.main()
  ```

- ##### 代码复用 - 调试时执行某一行特定的代码以及Cookie的存储、使用

  - 需要将Chromedriver&Chrome浏览器添加到环境变量

    windows：`chrome --remote-debugging-port=9222`
    mac：`Google\ Chrome --remote-debugging-port=9222`

  - CMD指定对应的命令，然后打开浏览器输入`127.0.0.1:9222`，页面可以正常打开说明Debugger启用正常

  - 
  
  - 进入复用模式 -> 进入对应的地址扫码登录 -> 获取Cookie并存储使用
  
    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/24 21:03
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_debuger.py
    # @software: PyCharm
    # 文件注释  : `chrome --remote-debugging-port=9222`
    import logging
    import time
    import yaml
    import pytest
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    
    
    class TestDebugger(object):
    
        """
        使用复用模式获取Cookie,需要先扫码登陆
        """
        def test_debugger(self):
            option = webdriver.ChromeOptions()
            option.debugger_address = '127.0.0.1:9222'
            driver = webdriver.Chrome(options=option)
            driver.get('https://work.weixin.qq.com/wework_admin/frame')
            with open('./cookie.yaml', 'w', encoding='utf-8') as f:
                print(driver.get_cookies())
                # Yaml写入需要传入data&stream
                yaml.safe_dump(driver.get_cookies(), f)
    
        """
        读取Yaml中的Cookie传入登录页面进行调用
        """
        def test_login(self):
            driver = webdriver.Chrome()
            # 窗口最大化，否则会造成元素找不到
            driver.maximize_window()
            # 首先要给出一个地址，否则会走默认data;
            driver.get('https://work.weixin.qq.com/')
            # 读取Cookie
            with open('./cookie.yaml', 'r', encoding='utf-8') as f:
                cookies = yaml.safe_load(f)
            # 循环遍历cookie并传入
            for cookie in cookies:
                driver.add_cookie(cookie)
            driver.get('https://work.weixin.qq.com/wework_admin/frame')
            driver.find_element(By.ID, 'menu_contacts').click()
            time.sleep(3)
    
    
    if __name__ == '__main__':
        pytest.main()
    ```
  
- ##### selenium中调用JS

  execute_script：执行JS

  return：返回JS的返回结果

  execute_script：arguments传参

  - 上滑操作
    
    - ```python
      driver.execute_script('document.documentElement.scrollTop=1000')
      ```
    
    - ```python
      driver.execute_script('return document.documentElement.scrollTop=1000')
      ```

- ##### 文件上传的操作

  - input标签可以直接使用send_keys(文件地址)上传文件

    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/26 20:58
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_uploadfile.py
    # @software: PyCharm
    # 文件注释  : 文件上传
    import time
    
    import pytest
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.wait import WebDriverWait
    
    
    class TestUploadFile(object):
    
        def test_upload(self):
            driver = webdriver.Chrome()
            driver.get('https://image.baidu.com/')
            driver.maximize_window()
            driver.implicitly_wait(10)
            driver.find_element(By.CLASS_NAME, 'st_camera_off').click()
            # 通过send_keys(文件路径)的方式上传文件
            driver.find_element(By.ID, 'stfile').send_keys(r'F:\Practice\hogwarts-pytest\1.png')
            time.sleep(10)
    
    
    if __name__ == '__main__':
        pytest.main()
    ```

- ##### 弹窗处理机制

  - switch_to.alert()进行定位

    - switch_to.alert()

      获取当前页面上的警告框

    - text

      返回alert/confirm/prompt中的文字信息

    - accept()

      接受现有警告框

    - dismiss()

      忽略现有的警告框

    - send_keys(keysToSend)

      发送文本至警告框

    ```python
    # -*- coding: UTF-8 -*-
    # @Time    : 2021/6/26 20:58
    # @Author  : Sun
    # @Email   : xxxxxxxxxxx@xxx.com
    # @File    : test_uploadfile.py
    # @software: PyCharm
    # 文件注释  : 文件上传
    import logging
    import time
    
    import pytest
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.wait import WebDriverWait
    
    
    class TestAlert(object):
    
        def test_alert(self):
            driver = webdriver.Chrome()
            driver.get('https://image.baidu.com/')
            driver.maximize_window()
            driver.implicitly_wait(10)
            driver.execute_script('window.alert("Alert测试弹窗")')
            # 获取alert弹窗文本
            logging.info(driver.switch_to.alert.text)
            # 返回alert/confirm/prompt中的文字信息
            driver.switch_to.alert.accept()
            time.sleep(5)
    
    
    if __name__ == '__main__':
        pytest.main()
    ```

