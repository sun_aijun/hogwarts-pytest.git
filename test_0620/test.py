# -*- coding:utf-8 -*-
import pytest
import yaml
from tested_code.calculator import Calculator


@pytest.fixture(scope='module')
def init_cal():
    cal = Calculator()
    return cal


@pytest.fixture(scope='function')
def end_cal():
    pass


@pytest.fixture(scope='function')
def start_cal():
    print("开始计算")
    yield end_cal
    print("结束计算")


# 定义Yaml读取的方法
"""
fixture方法不可以用来打开文件操作？
    Failed: Fixture "read_yaml" called directly. Fixtures are not meant to be called directly,
but are created automatically when test functions request them as parameters.
"""

# @pytest.fixture(scope='module')
def read_yaml():
    with open(r'../test_yaml/test.yaml') as f:
        return yaml.safe_load(f)


@pytest.fixture(scope='module', params=read_yaml()['add']['success'], ids=read_yaml()['add']['successIds'])
def get_add(request):
    result = request.param
    yield result


def test_a(get_add):
    pass