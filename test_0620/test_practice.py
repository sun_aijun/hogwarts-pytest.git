# -*- coding:utf-8 -*-
import pytest
import yaml
import allure
import logging


def read_yaml():
    with open(r'../test_yaml/test.yaml') as f:
        return yaml.safe_load(f)


@allure.feature("Pytest练习")
class TestPractice:

    TEST_CASE_LINK = "https://www.baidu.com"
    @allure.testcase(TEST_CASE_LINK, "加法运算测试用例链接")
    @allure.title("suite-成功的加法")
    @allure.story("成功的加法")
    @pytest.mark.add
    @pytest.mark.parametrize('a,b,expect', read_yaml()['add']['success'], ids=read_yaml()['add']['successIds'])
    def test_add_success(self, a, b, expect, init_cal, start_cal):
        logging.info(f"{a}+{b}")
        with allure.step("step-成功的加法运算"):
            allure.attach.file("../pic.JPG", "海绵宝宝", attachment_type=allure.attachment_type.JPG)
            assert expect == init_cal.add(a, b)

    @allure.story("失败的加法")
    @allure.title("suite-失败的加法")
    @pytest.mark.add
    @pytest.mark.parametrize('a,b,expect', read_yaml()['add']['fail'], ids=read_yaml()['add']['failIds'])
    def test_add_fail(self, a, b, expect, init_cal, start_cal):
        logging.info(f"{a}+{b}")
        with allure.step("step-失败的加法运算"):
            with pytest.raises(TypeError):
                assert expect == init_cal.add(a, b)

    @allure.story("成功的除法")
    @allure.title("suite-成功的除法")
    @pytest.mark.div
    @pytest.mark.parametrize('a,b,expect', read_yaml()['div']['success'], ids=read_yaml()['div']['successIds'])
    def test_div_success(self, a, b, expect, init_cal, start_cal):
        logging.info(f"{a}/{b}")
        with allure.step("step-成功的除法运算"):
            allure.attach("这是一个文本", "文本", attachment_type=allure.attachment_type.TEXT)
            assert expect == init_cal.div(a, b)

    @allure.story("失败的除法")
    @allure.title("suite-成功的除法")
    @pytest.mark.div
    @pytest.mark.parametrize('a,b,expect', read_yaml()['div']['fail'], ids=read_yaml()['div']['failIds'])
    def test_div_fail(self, a, b, expect, init_cal, start_cal):
        logging.info(f"{a}/{b}")
        with allure.step("step-失败的除法运算"):
            with pytest.raises(AssertionError):
                assert expect == init_cal.add(a, b)


if __name__ == '__main__':
    pytest.main()
