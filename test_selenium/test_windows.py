# -*- coding: UTF-8 -*-
# @Time    : 2021/6/23 22:50
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_windows.py
# @software: PyCharm
# 文件注释  : selenium的多窗口处理
import time
import logging
import pytest
from selenium.webdriver.common.by import By


class TestWindows(object):

    url = [['https://www.baidu.com', 'Chrome']]
    @pytest.mark.parametrize('get_driver', url, indirect=True)
    def test_windows(self, get_driver):
        get_driver.find_element(By.ID, 's-top-loginbtn').click()
        # current_window_handle方法获取当前所有窗口
        logging.info(get_driver.window_handles)
        # current_window_handle方法获取当前窗口
        logging.info(get_driver.current_window_handle)
        get_driver.find_element(By.CLASS_NAME, 'pass-reglink').click()
        # info操作拼接可以用格式化方式传入
        logging.info(f'打开注册页面后的窗口列表:{get_driver.window_handles}')
        logging.info(f'打开注册页面后当前所处的窗口:{get_driver.current_window_handle}')
        # 跳转到注册窗口
        get_driver.switch_to_window(get_driver.window_handles[-1])
        get_driver.find_element(By.ID, 'TANGRAM__PSP_4__userName').send_keys('张铁锤')
        get_driver.find_element(By.ID, 'TANGRAM__PSP_4__phone').send_keys('15235535702')
        # 切换回首页
        get_driver.switch_to_window(get_driver.window_handles[0])
        time.sleep(3)



if __name__ == '__main__':
    pytest.main()
