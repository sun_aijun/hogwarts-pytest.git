# -*- coding: UTF-8 -*-
# @Time    : 2021/6/26 20:58
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_uploadfile.py
# @software: PyCharm
# 文件注释  : 文件上传
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class TestUploadFile(object):

    def test_upload(self):
        driver = webdriver.Chrome()
        driver.get('https://image.baidu.com/')
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.find_element(By.CLASS_NAME, 'st_camera_off').click()
        # 通过send_keys(文件路径)的方式上传文件
        driver.find_element(By.ID, 'stfile').send_keys(r'F:\Practice\hogwarts-pytest\1.png')
        time.sleep(10)


if __name__ == '__main__':
    pytest.main()
