# -*- coding: UTF-8 -*-
# @Time    : 2021/6/24 21:03
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_debugger.py
# @software: PyCharm
# 文件注释  : `chrome --remote-debugging-port=9222`
import logging
import time
import yaml
import pytest
import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common import exceptions as ex
from selenium.webdriver.support.wait import WebDriverWait


@allure.story('添加用户')
class TestDebugger(object):

    """
    使用复用模式获取Cookie,需要先扫码登陆
    """
    @allure.title('进行Cookie的复用')
    def test_debugger(self):
        option = webdriver.ChromeOptions()
        option.debugger_address = '127.0.0.1:9222'
        driver = webdriver.Chrome(options=option)
        driver.get('https://work.weixin.qq.com/wework_admin/frame')
        with open('./cookie.yaml', 'w', encoding='utf-8') as f:
            print(driver.get_cookies())
            # Yaml写入需要传入data&stream
            yaml.safe_dump(driver.get_cookies(), f)

    """
    读取Yaml中的Cookie传入登录页面进行调用
    """
    @allure.title('添加用户的流程')
    def test_login(self):
        driver = webdriver.Chrome()
        driver.implicitly_wait(10)
        # 窗口最大化，否则会造成元素找不到
        driver.maximize_window()
        # 首先要给出一个地址，否则会走默认data;
        driver.get('https://work.weixin.qq.com/')
        # 读取Cookie
        with open('./cookie.yaml', 'r', encoding='utf-8') as f:
            cookies = yaml.safe_load(f)
        # 循环遍历cookie并传入
        for cookie in cookies:
            driver.add_cookie(cookie)
        driver.get('https://work.weixin.qq.com/wework_admin/frame')
        driver.find_element(By.ID, 'menu_contacts').click()
        """
        必须加一个sleep，否则代码执行时的页面还停留在上一个页面
        """
        time.sleep(5)
        # 点击添加成员
        driver.save_screenshot('./Screen/ClickAdd.png')
        with allure.step('点击添加成员'):
            allure.attach.file("./Screen/ClickAdd.png", "ClickAdd.png", attachment_type=allure.attachment_type.PNG)
            driver.find_element(By.XPATH, '//*[@class="js_has_member"]/div[1]/a[1]').click()
        # 输入信息
        with allure.step('输入用户名'):
            driver.find_element(By.ID, 'username').send_keys('张二狗')
        with allure.step('输入别名'):
            driver.find_element(By.ID, 'memberAdd_english_name').send_keys('二狗子')
        with allure.step('输入账号'):
            driver.find_element(By.ID, 'memberAdd_acctid').send_keys(17636330727)
        with allure.step('输入手机号'):
            driver.find_element(By.ID, 'memberAdd_phone').send_keys(17636330727)
        with allure.step('输入职务'):
            driver.find_element(By.ID, 'memberAdd_title').send_keys('搬砖')
        with allure.step('上滑页面'):
            driver.execute_script('document.documentElement.scrollTop=1000')
        driver.save_screenshot('./Screen/ClickCheckBox.png')
        with allure.step('取消复选'):
            allure.attach.file("././Screen/ClickCheckBox.png", "ClickCheckBox.png", attachment_type=allure.attachment_type.PNG)
            checkbox = driver.find_elements(By.XPATH, '//*[@class="ww_checkbox"]')
            logging.info(checkbox)
            # 拿到elements列表然后进行操作
            checkbox[4].click()
        # with allure.step('点击保存'):
        #     driver.find_element(By.LINK_TEXT, '保存').click()


if __name__ == '__main__':
    pytest.main()
