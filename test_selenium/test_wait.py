# -*- coding: UTF-8 -*-
# @Time    : 2021/6/22 12:37
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_wait.py
# @software: PyCharm
# 文件注释  : selenium显示等待
import time
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


class TestWait:

    url = [['https://www.baidu.com', 'Chrome']]
    @pytest.mark.parametrize('get_driver', url, indirect=True)
    def test_pra(self, get_driver):
        """
        def find_element(self, by=By.ID, value=None):
        编译器会提示调换参数位置，不能替换，会报错找不到元素
        """
        get_driver.find_element(By.ID, 'kw').send_keys('Sun')
        get_driver.find_element(By.ID, 'su').click()
        """
        By方法单独在一个括号里(By.LINK_TEXT, 'SUN(美国互联网技术服务公司) - 百度百科')
        until里面可以传expected_conditions方法去判断元素的状态
        """
        WebDriverWait(get_driver, 10).until(expected_conditions.presence_of_element_located((By.LINK_TEXT, 'SUN(美国互联网技术服务公司) - 百度百科')))


if __name__ == '__main__':
    pytest.main()
