# -*- coding: UTF-8 -*-
# @Time    : 2021/6/26 20:58
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_uploadfile.py
# @software: PyCharm
# 文件注释  : 文件上传
import logging
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class TestAlert(object):

    def test_alert(self):
        driver = webdriver.Chrome()
        driver.get('https://image.baidu.com/')
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.execute_script('window.alert("Alert测试弹窗")')
        # 获取alert弹窗文本
        logging.info(driver.switch_to.alert.text)
        # 返回alert/confirm/prompt中的文字信息
        driver.switch_to.alert.accept()
        time.sleep(5)


if __name__ == '__main__':
    pytest.main()
