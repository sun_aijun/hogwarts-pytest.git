# -*- coding: UTF-8 -*-
# @Time    : 2021/6/21 22:51
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_selenium.py
# @software: PyCharm
# 文件注释  : selenium的练习
import time
import pytest
import selenium
from selenium import webdriver
# selenium测试类
from selenium.webdriver.common.by import By


class TestSelenium(object):

    url = [['https://www.baidu.com', 'Chrome']]
    @pytest.mark.parametrize('get_driver', url, indirect=True)
    def test_selenium(self, get_driver):
        """
        使用By.ID需要导入
        from selenium.webdriver.common.by import By
        """
        get_driver.find_element(By.ID, 'kw').send_keys('Sun')
        get_driver.find_element(By.ID, 'su').click()
        # 含有<a/>可以使用By.By.LINK_TEXT
        get_driver.find_element(By.LINK_TEXT, '太阳石油[SUN]美股实时行情 - 富途牛牛').click()
        time.sleep(5)



if __name__ == '__main__':
    pytest.main('test_selenium.py')
