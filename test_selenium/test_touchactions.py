# -*- coding: UTF-8 -*-
# @Time    : 2021/6/23 9:07
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_actionchain.py
# @software: PyCharm
# 文件注释  : Actions操作
import time
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver import TouchActions


class TestActions(object):

    url = [['https://www.baidu.com', 'Chrome']]
    @pytest.mark.parametrize('get_driver', url, indirect=True)
    def test_touchaction(self, touch_driver):
        # 实例化Action
        action = TouchActions(touch_driver)
        # 收集action对象
        action.scroll_from_element(touch_driver.find_element(By.ID, 'kw'), 10000, 0)
        # 调用peform执行操作
        action.perform()
        time.sleep(5)


if __name__ == '__main__':
    pytest.main()
