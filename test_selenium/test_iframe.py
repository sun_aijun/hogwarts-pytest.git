# -*- coding: UTF-8 -*-
# @Time    : 2021/6/23 23:32
# @Author  : Sun
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test_iframe.py
# @software: PyCharm
# 文件注释  : iframe元素调用
# 测试地址  : https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable
import logging
import pytest
import logging
from selenium import webdriver
from selenium.webdriver.common.by import By


class TestFrame(object):

    """
    Fixture方法传参：
        通过parametrize把fixture方法传入，要传入fixture方法的参数当argvalues传入([[]])，测试方法再调用fixture方法
        添加indirect=True参数是为了把login当成一个函数去执行，而不是一个参数。
    """
    url = [['https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable', 'Chrome']]
    @pytest.mark.parametrize('get_driver', url, indirect=True)
    def test_frame(self, get_driver):
        # 进入iframe布局
        get_driver.switch_to_frame('iframeResult')
        logging.info('Switch进入成功')
        # 跳出iframe布局，switch_to_default_content代表进入此页面的默认布局
        get_driver.switch_to_default_content()
        logging.info('Switch退出成功')


if __name__ == '__main__':
    pytest.main()
